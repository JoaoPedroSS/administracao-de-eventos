import { Switch, Route } from "react-router-dom";
import Cardapio from "../pages/Cardapio";
import Casamento from "../pages/Casamento";
import Confraternizacao from "../pages/Confraternizacao";
import Formatura from "../pages/Formatura";

const Routes = () => {
  return (
    <Switch>
       <Route exact path="/">
           <Cardapio />
       </Route>
       <Route path="/casamento">
           <Casamento />
       </Route>
       <Route path="/confraternização">
           <Confraternizacao />
       </Route>
       <Route path="/formatura">
           <Formatura />
       </Route>
    </Switch>
  );
};

export default Routes;
