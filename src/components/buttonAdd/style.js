import styled from "styled-components";

export const ButtonAdd = styled.button`
  background-color: blue;
  color: black;
  border: 0;
  font-size: 0.53rem;
  padding: 0.25rem 1rem;
  border-radius: 5px;
  margin: 3px;
  cursor: pointer;
  font-weight: bold;
  width: 90px;

  @media (min-width: 800px) {
    background-color: blue;
    color: black;
    border: 0;
    font-size: 1rem;
    padding: 0.25rem 1rem;
    border-radius: 5px;
    margin: 10px;
    cursor: pointer;
    font-weight: bold;
    width: 200px;
  }
`;
