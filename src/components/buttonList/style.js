import styled from "styled-components";

export const ButtonPerson = styled.button`
  background-color: blue;
  color: black;
  border: 0;
  font-size: 1rem;
  padding: 0.25rem 1rem;
  border-radius: 5px;
  margin: 10px;
  cursor: pointer;
  font-weight: bold;
`;
