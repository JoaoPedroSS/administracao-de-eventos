import styled from "styled-components";

export const BoxProducts = styled.div`
  background-color: rgb(197, 156, 19);
  width: 250px;
  margin: 10px;
  height: 675px;
  border-radius: 7px;
  font-size: 20px;
  padding: 10px;

  @media (min-width: 800px) {
    background-color: gray;
    width: 450px;
    margin: 10px;
    height: 675px;
    border-radius: 7px;
    font-size: 20px;
    padding: 10px;
  }
`;
