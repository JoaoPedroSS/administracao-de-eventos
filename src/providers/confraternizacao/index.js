
import { createContext, useState} from "react";

export const ConfraternizacaoContext = createContext();

export const ConfraternizacaoProvider = ({children}) => {
    
    const [confra, setConfra] = useState([]);

    const addConfra = (item) => {
        setConfra([...confra, item])
    }

    const removeToConfra = (item) => {
        const removeConfra = confra.filter(
            (confraRemove) => confraRemove.name !== item.name
        );
        setConfra(removeConfra);
    }

    return(
        <ConfraternizacaoContext.Provider value={{confra, addConfra, removeToConfra}}>
            {children}
        </ConfraternizacaoContext.Provider>
    )
}