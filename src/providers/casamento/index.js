import { createContext, useState } from "react";

export const CasamentoContext = createContext();

export const CasamentoProvider = ({children}) => {

    const [casamento, setCasamento] = useState([]);

    const addCasamento = (item) => {
        setCasamento([...casamento, item])
    }

    const removeToCasamento = (item) => {
        const removeCasamento = casamento.filter(
            (casamentoRemove) => casamentoRemove.name !== item.name
        );
        setCasamento(removeCasamento);
    }

    return(
        <CasamentoContext.Provider value={{casamento, addCasamento, removeToCasamento}}>
            {children}
        </CasamentoContext.Provider>
    )
}