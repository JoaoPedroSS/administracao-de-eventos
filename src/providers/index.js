import { CardapioProvider } from "./cardapio";
import { CasamentoProvider } from "./casamento";
import { ConfraternizacaoProvider } from "./confraternizacao";
import { FormaturaContext, FormaturaProvider } from "./formatura";

const Providers = ({ children }) => {
  return (
    <CardapioProvider>
      <CasamentoProvider>
        <ConfraternizacaoProvider>
          <FormaturaProvider>{children}</FormaturaProvider>
        </ConfraternizacaoProvider>
      </CasamentoProvider>
    </CardapioProvider>
  );
};

export default Providers;
