import { createContext, useState } from "react";

export const FormaturaContext = createContext();

export const FormaturaProvider = ({children}) => {

    const [formatura, setFormatura] = useState([]);

    const addFormatura = (item) => {
        setFormatura([...formatura, item]);
    }

    const removeToFormatura = (item) => {
        const formaturaRemove = formatura.filter(
            (removeFormatura) => removeFormatura.name !== item.name
        );
        setFormatura(formaturaRemove);
    }

    return(
        <FormaturaContext.Provider value={{formatura, addFormatura, removeToFormatura}}>
            {children}
        </FormaturaContext.Provider>
    )

} 