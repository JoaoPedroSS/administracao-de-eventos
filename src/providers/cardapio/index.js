import { createContext, useEffect, useState } from "react";

import axios from "axios";

export const CardapioContext = createContext();

export const CardapioProvider = ({children}) => {

    const [cardapio, setCardapio] = useState([]);

    const getCardapio = () => {
        axios.get("https://api.punkapi.com/v2/beers")
        .then((response) => setCardapio(response.data));
    }

    useEffect(() => {
        getCardapio();
    }, [])

    return(
        <CardapioContext.Provider value={{cardapio , getCardapio}}>
            {children}
        </CardapioContext.Provider>
    )
}