import { ConfraternizacaoContext } from "../../providers/confraternizacao";
import { useContext } from "react";
import { useHistory } from "react-router-dom";
import { ButtonPerson } from "../../components/buttonList/style";
import { ButtonAdd } from "../../components/buttonAdd/style";
import { BoxProducts } from "../../components/box/style";

const Confraternizacao = () => {
  const { confra, removeToConfra } = useContext(ConfraternizacaoContext);

  const history = useHistory();

  return (
    <div>
      <ButtonPerson onClick={() => history.push("/")}>Voltar</ButtonPerson>
      <h3>Total de intems: {confra.length}</h3>
      <div
        style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}
      >
        {confra.map((bebidas) => (
          <BoxProducts className="box-item" key={bebidas.id}>
            Nome: {bebidas.name} <br></br>
            <p style={{ fontSize: 12 }}>Descrição: {bebidas.description}</p>
            <p style={{ fontSize: 12 }}>Litros: {bebidas.abv} L</p>
            <p style={{ fontSize: 12 }}>
              Data de Fabricação: {bebidas.first_brewed}
            </p>
            <div
              style={{
                height: "55%",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <img
                style={{ width: 85 }}
                src={bebidas.image_url}
                alt={bebidas.name}
              ></img>
            </div>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <ButtonAdd onClick={() => removeToConfra(bebidas)}>
                remover da lista
              </ButtonAdd>
            </div>
          </BoxProducts>
        ))}
      </div>
    </div>
  );
};

export default Confraternizacao;
