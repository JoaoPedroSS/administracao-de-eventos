import { useHistory } from "react-router-dom";
import { CardapioContext } from "../../providers/cardapio";
import { CasamentoContext } from "../../providers/casamento";
import { ConfraternizacaoContext } from "../../providers/confraternizacao";
import { FormaturaContext } from "../../providers/formatura";
import { useContext } from "react";
import { ButtonPerson } from "../../components/buttonList/style";
import { ButtonAdd } from "../../components/buttonAdd/style";
import { BoxProducts } from "../../components/box/style";

const Cardapio = () => {
  const { cardapio } = useContext(CardapioContext);
  const { casamento, addCasamento } = useContext(CasamentoContext);
  const { confra, addConfra } = useContext(ConfraternizacaoContext);
  const { formatura, addFormatura } = useContext(FormaturaContext);

  const history = useHistory();

  return (
    <div>
      <div style={{ padding: "40px" }}>
        <ButtonPerson onClick={() => history.push("/casamento")}>
          Lista de Casamento {casamento.length}
        </ButtonPerson>
        <ButtonPerson onClick={() => history.push("/confraternização")}>
          Lista de Confraternização {confra.length}
        </ButtonPerson>
        <ButtonPerson onClick={() => history.push("/formatura")}>
          Lista de Formatura {formatura.length}
        </ButtonPerson>
      </div>
      <div
        style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}
      >
        {cardapio.map((bebidas) => (
          <BoxProducts key={bebidas.id}>
            Nome: {bebidas.name} <br></br>
            <p style={{ fontSize: 12 }}>Descrição: {bebidas.description}</p>
            <p style={{ fontSize: 12 }}>Litros: {bebidas.abv} L</p>
            <p style={{ fontSize: 12 }}>
              Data de Fabricação: {bebidas.first_brewed}
            </p>
            <div
              style={{
                height: "55%",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <img
                style={{ width: 85 }}
                src={bebidas.image_url}
                alt={bebidas.name}
              ></img>
            </div>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <ButtonAdd onClick={() => addCasamento(bebidas)}>
                Adicionar Casamento
              </ButtonAdd>
              <ButtonAdd onClick={() => addConfra(bebidas)}>
                Adicionar Confraternização
              </ButtonAdd>
              <ButtonAdd onClick={() => addFormatura(bebidas)}>
                Adicionar Formatura
              </ButtonAdd>
            </div>
          </BoxProducts>
        ))}
      </div>
    </div>
  );
};

export default Cardapio;
